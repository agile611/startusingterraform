# Domain - Deploying Infrastructure with Terraform

The code mentioned in this document are used in the HashiCorp Certified Terraform Associate 2020 video course.

# Document Mapper

| Sr No | Document Link |
| ------ | ------ |
| 1 | [Creating first EC2 instance with Terraform](https://bitbucket.org/agile611/startusingterraform/src/afb44b46fa25c47efc359385227ca9438bf728d3/Section%201%20-%20Deploying%20Infrastructure%20with%20Terraform/first-ec2.md) |
| 2 | [Understanding Resource & Providers - Part 1](https://bitbucket.org/agile611/startusingterraform/src/afb44b46fa25c47efc359385227ca9438bf728d3/Section%201%20-%20Deploying%20Infrastructure%20with%20Terraform/resp01.md) |
| 3 | [Understanding Resource & Providers - Part 2](https://bitbucket.org/agile611/startusingterraform/src/afb44b46fa25c47efc359385227ca9438bf728d3/Section%201%20-%20Deploying%20Infrastructure%20with%20Terraform/github.md) |
| 4 | [Destroying Infrastructure with Terraform](https://bitbucket.org/agile611/startusingterraform/src/afb44b46fa25c47efc359385227ca9438bf728d3/Section%201%20-%20Deploying%20Infrastructure%20with%20Terraform/destroy.md) |
| 5 | [Terraform Provider Versioning](https://bitbucket.org/agile611/startusingterraform/src/afb44b46fa25c47efc359385227ca9438bf728d3/Section%201%20-%20Deploying%20Infrastructure%20with%20Terraform/provider-versioning.md) |


