# Domain  - Terraform Cloud & Enterprise Capabilities

The code mentioned in this document are used in the HashiCorp Certified Terraform Associate 2020 video course.


# Video-Document Mapper

| Sr No | Document Link |
| ------ | ------ |
| 1 | [Overview of Sentinel][PlDa] |
| 2 | [Overview of Remote Backends][PlDb] |





   [PlDa]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%207%20-%20Terraform%20Cloud%20%26%20Enterprise%20Capabilities/sentinel-policy.tf>
   [PlDb]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%207%20-%20Terraform%20Cloud%20%26%20Enterprise%20Capabilities/remote-backend.md>

