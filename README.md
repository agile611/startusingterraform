[![Agile611](https://www.agile611.com/wp-content/uploads/2020/09/cropped-logo-header.png)](http://www.agile611.com/)

# Start Using Terraform

Here are there is a demo from the architecture shown in the training.

## Installation for Local Development

Setup an Ubuntu 18.04 VM (from now on the Box) using the following command:

```shell
    vagrant up
```

The box contains Terraform engine from linux. Additionally it also contains packer.

## Problems provisioning the box

If you have problems provisioning the box, you can download it directly from [here](https://app.vagrantup.com/bento/boxes/ubuntu-20.04/versions/202112.19.0/providers/virtualbox.box)

After that you need to know the path of the box and execute the following command:

```shell
    vagrant box add /The/Path/From/Your/Downloaded/box/bento-ubuntu-20-04.box --name bento/ubuntu-20.04
    vagrant init bento/ubuntu-20.04
```

The init command creates a VagrantFile with your initial configuration. On the same folder where this Vagrantfile is, please execute to following command:

```shell
    vagrant up
```

After that, please connect to the box using the following command:

```shell
    vagrant ssh
```

If you get a terminal from the box, your environment is ready.

## Common networking problems

If you have proxies or VPNs running on your machine, it is possible that Vagrant is not able to provision your environment.

Please check your connectivity before.

## Support

This tutorial is released into the public domain by [Agile611](http://www.agile611.com/) under Creative Commons Attribution-NonCommercial 4.0 International.

[![License: CC BY-NC 4.0](https://img.shields.io/badge/License-CC_BY--NC_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc/4.0/)


This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhs/) and is likewise released into the public domain.

Please contact Agile611 for further details.

* [Agile611](http://www.agile611.com/)
* Laureà Miró 309
* 08950 Esplugues de Llobregat (Barcelona)