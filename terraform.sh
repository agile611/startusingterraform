#!/bin/bash
set -x

#Install Ansible
sudo apt-get update
sudo apt-get install software-properties-common -y
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt-get update
sudo apt-get install ansible -y
sudo apt-get update

#Install more things
apt-get -y install docker.io unzip ntpdate

# add docker privileges
usermod -G docker vagrant

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update -y

##Terraform
sudo apt-get install terraform -y

##Packer
sudo apt-get install packer -y