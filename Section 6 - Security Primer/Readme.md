# Domain  - Security Primer

The code mentioned in this document are used in the HashiCorp Certified Terraform Associate 2020 video course.


# Video-Document Mapper

| Sr No | Document Link |
| ------ | ------ |
| 1 | [Handling Access & Secret Keys the Right Way in Providers][PlDa] |
| 2 | [Terraform Provider UseCase - Resources in Multiple Regions][PlDb] |
| 3 | [Sensitive Parameter][PlDc] |
| 4 |[Security Challenges in Commiting TFState to GIT][PlDd] |





   [PlDa]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%206%20-%20Security%20Primer/credentials.md>
   [PlDb]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%206%20-%20Security%20Primer/multiple-providers.md>
   [PlDc]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%206%20-%20Security%20Primer/sensitive.tf>
   [PlDd]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%206%20-%20Security%20Primer/tfstate-git.md>

