### Jenkins packer demo build

NOTE: Important to adapt the scripts from the scripts/ folder using a new S3 bucket you already created.

Execute the following commands to provision a Jenkins using Terraform, type the following commands or convert it on a script:

```shell
ARTIFACT=`packer build -machine-readable packer-demo.json |awk -F, '$0 ~/artifact,0,id/ {print $6}'`
AMI_ID=`echo $ARTIFACT | cut -d ':' -f2`
echo 'variable "APP_INSTANCE_AMI" { default = "'${AMI_ID}'" }' > amivar.tf
aws s3 cp amivar.tf s3://terraform-your-s3-custom-bucket/amivar.tf
```

NOTE: Important to adapt the scripts from the scripts/ folder using a new S3 bucket you already created

### Jenkins terraform build

Execute the following commands to provision a Jenkins using Terraform, type the following commands or convert it on a script:

```shell
cd 008_jenkins_packer_demo
aws s3 cp s3://terraform-your-s3-custom-bucket/amivar amivar.tf
ssh-keygen -f mykey
terraform apply -auto-approve -var APP_INSTANCE_COUNT=1 -target aws_instance.app-instance
```
