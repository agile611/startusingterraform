terraform{
    backend "s3"{
        bucket = "YOUR_BUCKET_UNIQUE_NAME"
        key = "terraform/remotestate"
    }
}