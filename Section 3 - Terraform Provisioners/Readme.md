# Domain  - Terraform Provisioners

The code mentioned in this document are used in the HashiCorp Certified Terraform Associate 2020 video course.


# Video-Document Mapper

| Sr No | Document Link |
| ------ | ------ |
| 1 | [Implementing remote-exec provisioners][PlDa] |
| 2 | [Implementing local-exec provisioners][PlDb] |
| 3 | [Creation-Time & Destroy-Time Provisioners][PlDc] |
| 4 | [Failure Behavior for Provisioners][PlDd] |


[PlDa]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%203%20-%20Terraform%20Provisioners/remote-exec.tf>
[PlDb]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%203%20-%20Terraform%20Provisioners/local-exec.tf>
[PlDc]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%203%20-%20Terraform%20Provisioners/provisioner-types.md>
[PlDd]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%203%20-%20Terraform%20Provisioners/failure-behavior.md>
