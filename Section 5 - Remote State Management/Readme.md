# Domain - Remote State Management

The code mentioned in this document are used in the HashiCorp Certified Terraform Associate 2020 video course.


### Video-Document Mapper

| Sr No | Document Link |
| ------ | ------ |
| 1 | [Implementing S3 Backend][PlDa] |
| 2 | [Importing Existing Resources with Terraform Import][PlDb] |
| 3 | [Terraform State Management][PlDc] |
| 4 | [Integrating with GIT for team management][PlDd] |
| 5 | [Module Sources in Terraform][PlDe] |
| 6 | [Terraform and .gitignore][PlDf] |


   [PlDa]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%205%20-%20Remote%20State%20Management/kplabs-remote-backend>
   [PlDb]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%205%20-%20Remote%20State%20Management/tf-import.md>
   [PlDc]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%205%20-%20Remote%20State%20Management/state-management.md>
   [PlDd]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%205%20-%20Remote%20State%20Management/git-integration.md>   
   [PlDe]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%205%20-%20Remote%20State%20Management/demofile.md>
   [PlDf]: <https://bitbucket.org/agile611/startusingterraform/src/master/Section%205%20-%20Remote%20State%20Management/tf-gitignore.md>
